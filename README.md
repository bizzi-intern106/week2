# ASSIGNMENT WEEK 2

### FUNCTION
- Using Graphql and Apollo server
- Sign up, login flow
- Dependency hierarchy
- Unit Testing

### USAGE
- install package dependencies
> npm install
- build server
> npm run build
- start server
> npm start
- test server
> npm run test

### SWAGGER
- *http://localhost/docs*
