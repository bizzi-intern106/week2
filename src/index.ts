import "module-alias/register";
import 'dotenv/config';
import Fastify from 'fastify'
import resolvePlugin from "./bootstrap";
import resolveApollo from "./apollo";

//tạo fastify server
const fastify = Fastify({
    logger: process.env.NODE_ENV === "development" ? true : false
});

// load plugin
(async () => {
    resolvePlugin(fastify);
    await resolveApollo(fastify);
})()


const bootstrap = async () => {
    await fastify.listen({ port: 80 });
    return fastify;
}

bootstrap();
export default bootstrap;