import { FastifyInstance } from "fastify";
import fastifySwagger from "@fastify/swagger";
import fastifySwaggerUi from "@fastify/swagger-ui";
import DomainError from "@lib/error";
import { lowdbPlugin } from "@modules/auth/v2";
import { containerPlugin, tokenGeneratorPlugin } from "@plugin/index";

const swaggerOptions = {
    swagger: {
        info: {
            title: "My Title",
            description: "My Description.",
            version: "1.0.0",
        },
        host: "localhost",
        schemes: ["http", "https"],
        consumes: ["application/json"],
        produces: ["application/json"],
        tags: [{ name: "Default", description: "Default" }],
    },
};

const swaggerUiOptions = {
    routePrefix: "/docs",
    exposeRoute: true,
};

const resolvePlugin = (fastify: FastifyInstance) => {
    fastify.register(lowdbPlugin({ connectionString: "src/data/db1.json" }));
    fastify.register(tokenGeneratorPlugin({ key: "test...." }));
    fastify.register(fastifySwagger, swaggerOptions);
    fastify.register(fastifySwaggerUi, swaggerUiOptions);
    fastify.register(containerPlugin());
    fastify.setErrorHandler((error, _, reply) => {
        if (error instanceof DomainError) {
            reply.status(error.code)
                .send({
                    statusCode: error.code,
                    message: error.message,
                    error: "Domain throw error"
                })
        } else {
            reply.send(error);
        }
    })
}

export default resolvePlugin;