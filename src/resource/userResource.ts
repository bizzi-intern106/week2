export interface UserResponse {
    username: string,
    id?: string
}