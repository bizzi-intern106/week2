import { DbOptions } from "@modules/auth/v1/connection"
import { TokenOptions } from "@lib/token"

const config = {
    CONNECTION_STRING: process.env.CONNECTION_STRING,
    JWT_PRIVATE_KEY: process.env.JWT_PRIVATE_KEY
}

export const dbOptions: DbOptions = {
    connectionString: config.CONNECTION_STRING
}

export const tokenOptions: TokenOptions = {
    key: config.JWT_PRIVATE_KEY
}