import { ApolloServer, BaseContext } from "@apollo/server";
import fastifyApollo from "@as-integrations/fastify";
import { FastifyInstance, FastifyRequest } from "fastify";
import { typeDefs, userResolver } from "@modules/auth/v2";
import { GraphQLError } from "graphql";

export interface GraphQLContext extends BaseContext {
    req: FastifyRequest,
    fastify: FastifyInstance
}

const apollo = new ApolloServer<GraphQLContext>({
    typeDefs: typeDefs,
    resolvers: userResolver,
    status400ForVariableCoercionErrors: true
});


const resolveApollo = async (fastify: FastifyInstance) => {
    await apollo.start();
    await fastify.register(fastifyApollo(apollo), {
        context: async (req, reply) => {
            try {

                let token = req.headers["authorization"];
                if (token) {
                    //handle
                    token = token.replace("Bearer ", "");
                    const payload = fastify.token.verifyToken(token);
                    req.user = payload;
                    req.token = token;
                }
                return {
                    req,
                    fastify
                }
            } catch (err) {
                reply.status(200).send({
                    errors: [{
                        message: err.message,
                        extensions: {
                            code: "UNAUTHENTICATION"
                        }
                    }]
                });
            }
        }
    });
}


export default resolveApollo;

