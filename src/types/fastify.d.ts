import { TokenGenerator, TokenPayload } from "@lib/token";
import { ILowdbMethod } from "@modules/auth/v2/lowdbPlugin";
import { Container } from "@lib/container";

declare module 'fastify' {
    interface FastifyInstance {
        token: TokenGenerator,
        lowdb: ILowdbMethod,
        container: Container
    }

    interface FastifyRequest {
        token: string,
        user: TokenPayload
    }
}
