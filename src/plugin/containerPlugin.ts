import createContainer from "@root/lib/container";
import { FastifyPluginCallback } from "fastify";
import fp from "fastify-plugin";

import {
    DbConnection,
    DbOptions,
    IUserRepository,
    IUserService,
    UserRouter,
    createConnection,
    userRepository,
    userRouter,
    userService
} from "@modules/auth/v1";
import { dbOptions, tokenOptions } from "@config/config";
import createTokenGenerator, { TokenGenerator, TokenOptions } from "@lib/token";

const containerPlugin = () => {
    const container = createContainer();

    const plugin: FastifyPluginCallback = fp((fastify, opts, done) => {
        fastify.decorate("container", container);

        fastify.container.register<TokenGenerator>("tokenGenerator").to(createTokenGenerator);
        fastify.container.register<DbConnection>("dbConnection").to(createConnection);
        fastify.container.register<IUserRepository>("userRepository").to(userRepository);
        fastify.container.register<IUserService>("userService").to(userService);
        fastify.container.register<FastifyPluginCallback<UserRouter>>("userRouter").to(userRouter, { prefix: "/v1/auth" });
        fastify.container.bind<TokenOptions>("tokenOptions").to(tokenOptions);
        fastify.container.bind<DbOptions>("dbOptions").to(dbOptions);

        fastify.container.loadRoutes(fastify);
        done();
    })

    return plugin;
}

export default containerPlugin;