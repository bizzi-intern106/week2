import createTokenGenerator, { TokenOptions } from "@lib/token";
import { FastifyPluginCallback } from "fastify";
import fp from "fastify-plugin";

const tokenGeneratorPlugin = (tokenOptions: TokenOptions) => {
    const result = createTokenGenerator(tokenOptions);

    const plugin: FastifyPluginCallback = fp((fastify, opts, done) => {
        fastify.decorate("token", result);
        done();
    })

    return plugin;
}

export default tokenGeneratorPlugin;