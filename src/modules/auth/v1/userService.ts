import { BadRequestError } from "@lib/error"
import { compareData, hashData } from "@lib/hash";
import { TokenGeneration, TokenGenerator } from "@lib/token";
import User from "./user";
import { IUserRepository } from "./userRepository";
import { UserResponse } from "@root/resource/userResource";

export interface IUserService {
    get({ username }: Omit<User, 'password'>): Promise<{ user: UserResponse }>;
    signUp({ username, password }: User): Promise<{ user: UserResponse }>;
    login({ username, password }: User): Promise<{
        user: UserResponse
        token: TokenGeneration
    }>
}

const userService = (userRepository: IUserRepository, tokenGenerator: TokenGenerator): IUserService => {
    const result = {
        async get({ username }: Omit<User, 'password'>): Promise<{ user: UserResponse }> {
            const user = await userRepository.find({ username, password: "" });
            if (!user) {
                throw new BadRequestError(`user '${username}' does not existed`);
            }

            delete user.password;
            return { user };
        },
        async signUp({ username, password }: User): Promise<{ user: UserResponse }> {
            const user = await userRepository.find({ username, password });

            if (user) {
                throw new BadRequestError(`user ${username} has existed`);
            }

            const hashedPassword = await hashData(password);
            const _user = await userRepository.create({
                username,
                password: hashedPassword
            });

            delete _user.password;
            return { user: _user };
        },
        async login({ username, password }: User): Promise<{
            user: UserResponse,
            token: TokenGeneration
        }> {
            const user = await userRepository.find({ username, password });

            if (!user) {
                throw new BadRequestError(`user '${username}' does not existed`);
            }

            const isMatch = await compareData(password, user.password);
            if (!isMatch) {
                throw new BadRequestError(`password does not matched`);
            }

            const tokens = tokenGenerator.signToken({ username, id: "" });

            delete user.password;
            return {
                user,
                token: tokens
            }
        }
    }

    return result;
}

export default userService;