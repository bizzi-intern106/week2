import { BaseContext } from "@apollo/server";
import { FastifyInstance, FastifyRequest } from "fastify";
export interface GraphQLContext extends BaseContext {
    req: FastifyRequest;
    fastify: FastifyInstance;
}
declare const resolveApollo: (fastify: FastifyInstance) => Promise<void>;
export default resolveApollo;
