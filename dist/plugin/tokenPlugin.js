"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const token_1 = __importDefault(require("@lib/token"));
const fastify_plugin_1 = __importDefault(require("fastify-plugin"));
const tokenGeneratorPlugin = (tokenOptions) => {
    const result = (0, token_1.default)(tokenOptions);
    const plugin = (0, fastify_plugin_1.default)((fastify, opts, done) => {
        fastify.decorate("token", result);
        done();
    });
    return plugin;
};
exports.default = tokenGeneratorPlugin;
//# sourceMappingURL=tokenPlugin.js.map