import { FastifyPluginCallback } from "fastify";
declare const containerPlugin: () => FastifyPluginCallback;
export default containerPlugin;
