import { TokenOptions } from "@lib/token";
import { FastifyPluginCallback } from "fastify";
declare const tokenGeneratorPlugin: (tokenOptions: TokenOptions) => FastifyPluginCallback;
export default tokenGeneratorPlugin;
