"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const container_1 = __importDefault(require("@root/lib/container"));
const fastify_plugin_1 = __importDefault(require("fastify-plugin"));
const v1_1 = require("@modules/auth/v1");
const config_1 = require("@config/config");
const token_1 = __importDefault(require("@lib/token"));
const containerPlugin = () => {
    const container = (0, container_1.default)();
    const plugin = (0, fastify_plugin_1.default)((fastify, opts, done) => {
        fastify.decorate("container", container);
        fastify.container.register("tokenGenerator").to(token_1.default);
        fastify.container.register("dbConnection").to(v1_1.createConnection);
        fastify.container.register("userRepository").to(v1_1.userRepository);
        fastify.container.register("userService").to(v1_1.userService);
        fastify.container.register("userRouter").to(v1_1.userRouter, { prefix: "/v1/auth" });
        fastify.container.bind("tokenOptions").to(config_1.tokenOptions);
        fastify.container.bind("dbOptions").to(config_1.dbOptions);
        fastify.container.loadRoutes(fastify);
        done();
    });
    return plugin;
};
exports.default = containerPlugin;
//# sourceMappingURL=containerPlugin.js.map