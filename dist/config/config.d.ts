import { DbOptions } from "@modules/auth/v1/connection";
import { TokenOptions } from "@lib/token";
export declare const dbOptions: DbOptions;
export declare const tokenOptions: TokenOptions;
