"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("@root/utils");
const createContainer = () => {
    const records = {};
    const objectRecords = {};
    const prefixs = {};
    const result = {
        register(name) {
            const recordType = records[name];
            if (recordType) {
                throw new Error(`Type ${name} has registed`);
            }
            const result = {
                to(factory, opts) {
                    records[name] = factory;
                    if (opts) {
                        prefixs[name] = opts.prefix;
                    }
                }
            };
            return result;
        },
        bind(name) {
            const recordType = objectRecords[name];
            if (recordType) {
                throw new Error(`Type ${name} has registed`);
            }
            const result = {
                to(object) {
                    objectRecords[name] = object;
                }
            };
            return result;
        },
        resolve(name) {
            const object = objectRecords[name];
            if (object) {
                return object;
            }
            const factory = records[name];
            if (!factory) {
                return undefined;
            }
            const args = (0, utils_1.getArgumentNames)(factory);
            const dependencies = args.map(arg => (this.resolve(arg)));
            const result = factory(...dependencies);
            objectRecords[name] = result;
            return result;
        },
        unbind(name) {
            const factory = records[name];
            const object = objectRecords[name];
            if (!factory && !object) {
                throw new Error(`Type ${name} does not exist`);
            }
            if (factory) {
                delete records[name];
            }
            if (object) {
                delete objectRecords[name];
            }
        },
        loadRoutes(fastify) {
            Object.keys(records).forEach((name) => {
                if (name.includes("Router")) {
                    fastify.register(this.resolve(name), { prefix: prefixs[name] });
                }
            });
        }
    };
    return result;
};
exports.default = createContainer;
//# sourceMappingURL=container.js.map