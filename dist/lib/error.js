"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForBiddenError = exports.UnauthenticationError = exports.BadRequestError = void 0;
class DomainError extends Error {
    constructor(message, code) {
        super(message);
        this.code = code;
    }
}
exports.default = DomainError;
class BadRequestError extends DomainError {
    constructor(message) {
        super(message, 400);
    }
}
exports.BadRequestError = BadRequestError;
class UnauthenticationError extends DomainError {
    constructor(message) {
        super(message, 401);
    }
}
exports.UnauthenticationError = UnauthenticationError;
class ForBiddenError extends DomainError {
    constructor(message) {
        super(message, 403);
    }
}
exports.ForBiddenError = ForBiddenError;
//# sourceMappingURL=error.js.map