"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importStar(require("jsonwebtoken"));
const error_1 = require("./error");
const { sign, verify } = jsonwebtoken_1.default;
const createTokenGenerator = (tokenOptions) => {
    const result = {
        signToken(payload) {
            const accessToken = sign(payload, tokenOptions.key, { expiresIn: '10m' });
            const refreshToken = sign(payload, tokenOptions.key, { expiresIn: '1d' });
            const result = {
                accessToken,
                refreshToken
            };
            return result;
        },
        verifyToken(token) {
            try {
                const time = Date.now();
                const { exp } = (0, jsonwebtoken_1.decode)(token);
                if (exp * 1000 < time) {
                    throw new error_1.UnauthenticationError('token expiration');
                }
                const payload = verify(token, tokenOptions.key);
                const result = {
                    username: payload.username,
                    id: payload.id
                };
                return result;
            }
            catch (err) {
                throw new error_1.UnauthenticationError("cannot verify token because token invalid");
            }
        }
    };
    return result;
};
exports.default = createTokenGenerator;
//# sourceMappingURL=token.js.map