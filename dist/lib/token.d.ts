export type TokenOptions = {
    key: string;
};
export type TokenPayload = {
    username: string;
    id: string;
};
export type TokenGeneration = {
    refreshToken: string;
    accessToken: string;
};
export type TokenGenerator = {
    signToken: (payload: TokenPayload) => TokenGeneration;
    verifyToken: (token: string) => TokenPayload;
};
declare const createTokenGenerator: (tokenOptions: TokenOptions) => TokenGenerator;
export default createTokenGenerator;
