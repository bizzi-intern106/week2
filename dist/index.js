"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("module-alias/register");
require("dotenv/config");
const fastify_1 = __importDefault(require("fastify"));
const bootstrap_1 = __importDefault(require("./bootstrap"));
const apollo_1 = __importDefault(require("./apollo"));
const fastify = (0, fastify_1.default)({
    logger: process.env.NODE_ENV === "development" ? true : false
});
(async () => {
    (0, bootstrap_1.default)(fastify);
    await (0, apollo_1.default)(fastify);
})();
const bootstrap = async () => {
    await fastify.listen({ port: 80 });
    return fastify;
};
bootstrap();
exports.default = bootstrap;
//# sourceMappingURL=index.js.map