"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getArgumentType = exports.getArgumentNames = void 0;
function getArgumentNames(func) {
    const funcString = func.toString();
    const argNames = funcString.slice(funcString.indexOf('(') + 1, funcString.indexOf(')')).match(/([^\s,]+)/g);
    return argNames || [];
}
exports.getArgumentNames = getArgumentNames;
function getArgumentType(func) {
    const funcString = func.toString();
    const args = funcString.slice(funcString.indexOf('(') + 1, funcString.indexOf(')'));
    const result = [];
    const pattern = /(\w+):\s+(\w+)/g;
    let match;
    while ((match = pattern.exec(args)) !== null) {
        const argType = match[2];
        result.push(argType);
    }
    return result;
}
exports.getArgumentType = getArgumentType;
//# sourceMappingURL=index.js.map