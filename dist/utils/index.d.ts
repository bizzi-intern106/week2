export declare function getArgumentNames(func: (...args: any[]) => void): string[];
export declare function getArgumentType(func: (...args: any[]) => any): string[];
