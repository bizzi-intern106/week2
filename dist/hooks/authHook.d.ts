import { Container } from "@root/lib/container";
import { FastifyReply, FastifyRequest } from "fastify";
declare const authenticationHook: (container: Container) => (req: FastifyRequest, reply: FastifyReply, done: any) => Promise<void>;
declare const authorization: (req: FastifyRequest, reply: FastifyReply, done: any) => Promise<void>;
export { authenticationHook, authorization };
