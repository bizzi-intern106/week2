"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authorization = exports.authenticationHook = void 0;
const error_1 = require("@root/lib/error");
const authenticationHook = (container) => {
    const authentication = async (req, reply, done) => {
        let token = req.headers["authorization"];
        if (!token) {
            throw new error_1.UnauthenticationError("token was not appear on request header");
        }
        token = token.replace("Bearer ", "");
        const tokenGenerator = container.resolve("tokenGenerator");
        const payload = tokenGenerator.verifyToken(token);
        req.user = payload;
        req.token = token;
        done();
    };
    return authentication;
};
exports.authenticationHook = authenticationHook;
const authorization = async (req, reply, done) => {
};
exports.authorization = authorization;
//# sourceMappingURL=authHook.js.map