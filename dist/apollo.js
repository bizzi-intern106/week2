"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("@apollo/server");
const fastify_1 = __importDefault(require("@as-integrations/fastify"));
const v2_1 = require("@modules/auth/v2");
const apollo = new server_1.ApolloServer({
    typeDefs: v2_1.typeDefs,
    resolvers: v2_1.userResolver,
    status400ForVariableCoercionErrors: true
});
const resolveApollo = async (fastify) => {
    await apollo.start();
    await fastify.register((0, fastify_1.default)(apollo), {
        context: async (req, reply) => {
            try {
                let token = req.headers["authorization"];
                if (token) {
                    token = token.replace("Bearer ", "");
                    const payload = fastify.token.verifyToken(token);
                    req.user = payload;
                    req.token = token;
                }
                return {
                    req,
                    fastify
                };
            }
            catch (err) {
                reply.status(200).send({
                    errors: [{
                            message: err.message,
                            extensions: {
                                code: "UNAUTHENTICATION"
                            }
                        }]
                });
            }
        }
    });
};
exports.default = resolveApollo;
//# sourceMappingURL=apollo.js.map