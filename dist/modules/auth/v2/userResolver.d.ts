import { GraphQLContext } from "@root/apollo";
declare const userResolver: {
    Mutation: {
        signUp(_: any, args: any, { fastify }: GraphQLContext): Promise<import("./user").default>;
    };
    Query: {
        get(_: any, __: any, { fastify, req }: GraphQLContext): Promise<import("./user").default>;
        login(_: any, args: any, { fastify }: GraphQLContext): Promise<{
            token: {
                refreshToken: string;
                accessToken: string;
            };
            user: {
                id: string;
                username: string;
                password: string;
            };
        }>;
    };
};
export default userResolver;
