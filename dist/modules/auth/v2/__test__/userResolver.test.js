"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = __importDefault(require("@root/index"));
const user_fixture_1 = require("./fixtures/user.fixture");
describe("[TEST] Testing userResolver functionalities", () => {
    let fastify;
    beforeAll(async () => {
        fastify = await (0, index_1.default)();
    });
    afterAll(() => {
        fastify.close();
    });
    describe("[TEST] /graphql signUp", () => {
        const signUpMutation = `
                mutation SignUp($user: UserInput) {
                    signUp(user: $user) {
                        id,
                        username
                    }
                }
            `;
        const userInput = {
            user: {
                username: user_fixture_1.CREATE_USER_DATA.username,
                password: user_fixture_1.CREATE_USER_DATA.password
            }
        };
        test("[FUNCTION] Testing signup while username has existed on database", async () => {
            jest.spyOn(fastify.lowdb, 'find')
                .mockImplementation(() => Promise.resolve({ ...user_fixture_1.CREATE_USER_DATA }));
            const response = await fastify.inject({
                method: "POST",
                url: "/graphql",
                body: {
                    query: signUpMutation,
                    variables: {
                        ...userInput
                    }
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.data.signUp).toBeNull();
            expect(json.errors).toBeTruthy();
        });
        test("[FUNCTION] Testing signup while username does not exist on database", async () => {
            jest.spyOn(fastify.lowdb, 'find')
                .mockImplementation(() => Promise.resolve(undefined));
            jest.spyOn(fastify.lowdb, 'create')
                .mockImplementation(() => Promise.resolve({ ...user_fixture_1.CREATE_USER_DATA }));
            const response = await fastify.inject({
                method: 'POST',
                url: "/graphql",
                body: {
                    query: signUpMutation,
                    variables: {
                        ...userInput
                    }
                }
            });
            expect(fastify.lowdb.create).toHaveBeenCalled();
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.data.signUp).toBeTruthy();
            expect(json.data.signUp.username).toBe(user_fixture_1.CREATE_USER_DATA.username);
        });
    });
    describe("[TEST] /graphql login", () => {
        const loginQuery = `
            query Login($user: UserInput) {
                login(user: $user) {
                    user {
                        username
                    }
                    token {
                        accessToken
                    }
                }
            }`;
        const userInput = {
            user: {
                username: user_fixture_1.CREATE_USER_DATA.username,
                password: user_fixture_1.CREATE_USER_DATA.password
            }
        };
        test("[FUNCTION] Testing login while user does not exist on database", async () => {
            jest.spyOn(fastify.lowdb, 'find')
                .mockImplementation(() => Promise.resolve(undefined));
            const response = await fastify.inject({
                method: "POST",
                url: "/graphql",
                body: {
                    query: loginQuery,
                    variables: {
                        ...userInput
                    }
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.data.login).toBeNull();
            expect(json.errors).toBeTruthy();
        });
        test("[FUNCTION] Testing login while user has existed on database but password does not match", async () => {
            jest.spyOn(fastify.lowdb, 'find')
                .mockImplementation(() => Promise.resolve({ ...user_fixture_1.CREATE_USER_DATA }));
            jest.spyOn(await Promise.resolve().then(() => __importStar(require("@lib/hash"))), 'compareData')
                .mockResolvedValue(false);
            const response = await fastify.inject({
                method: "POST",
                url: "/graphql",
                body: {
                    query: loginQuery,
                    variables: {
                        ...userInput
                    }
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.data.login).toBeNull();
            expect(json.errors).toBeTruthy();
        });
        test("[FUNCTION] Testing login while user has existed on database and password is matched", async () => {
            jest.spyOn(fastify.lowdb, 'find')
                .mockImplementation(() => Promise.resolve({ ...user_fixture_1.CREATE_USER_DATA }));
            jest.spyOn(await Promise.resolve().then(() => __importStar(require("@lib/hash"))), 'compareData')
                .mockResolvedValue(true);
            jest.spyOn(fastify.token, 'signToken')
                .mockReturnValue({ accessToken: "", refreshToken: "" });
            const response = await fastify.inject({
                method: "POST",
                url: "/graphql",
                body: {
                    query: loginQuery,
                    variables: {
                        ...userInput
                    }
                }
            });
            expect(fastify.token.signToken).toHaveBeenCalled();
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.data.login).toBeTruthy();
            expect(json.data.login.user.username).toEqual(user_fixture_1.CREATE_USER_DATA.username);
        });
    });
    describe("[TEST] /graphql get", () => {
        const getUserQuery = `
            query Get {
                get {
                    id,
                    username
                }
            }`;
        const token = "Bearer token";
        test("[FUNCTION] Testing get user while empty header authorization", async () => {
            const response = await fastify.inject({
                method: "POST",
                url: "/graphql",
                body: {
                    query: getUserQuery,
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.errors).toBeTruthy();
        });
        test("[FUNCTION] Testing get user while invalid token", async () => {
            const response = await fastify.inject({
                method: "POST",
                body: {
                    query: getUserQuery,
                },
                url: "/graphql",
                headers: {
                    authorization: token
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.errors).toBeTruthy();
        });
        test("[FUNCTION] Testing get user while user id attach on token does not appear on database", async () => {
            jest.spyOn(fastify.token, 'verifyToken')
                .mockReturnValue({ ...user_fixture_1.CREATE_USER_DATA });
            jest.spyOn(fastify.lowdb, 'find')
                .mockReturnValue(Promise.resolve(undefined));
            const response = await fastify.inject({
                method: "POST",
                body: {
                    query: getUserQuery,
                },
                url: "/graphql",
                headers: {
                    authorization: token
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.errors).toBeTruthy();
        });
        test("[FUNCTION] Testing get user while user id attach on token does not appear on database", async () => {
            jest.spyOn(fastify.token, 'verifyToken')
                .mockReturnValue({ ...user_fixture_1.CREATE_USER_DATA });
            jest.spyOn(fastify.lowdb, 'find')
                .mockReturnValue(Promise.resolve({ ...user_fixture_1.CREATE_USER_DATA }));
            const response = await fastify.inject({
                method: "POST",
                body: {
                    query: getUserQuery,
                },
                url: "/graphql",
                headers: {
                    authorization: token
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.data.get).toBeTruthy();
            expect(json.data.get.username).toEqual(user_fixture_1.CREATE_USER_DATA.username);
        });
    });
});
//# sourceMappingURL=userResolver.test.js.map