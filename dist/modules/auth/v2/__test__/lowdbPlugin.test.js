"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_1 = __importDefault(require("fastify"));
const lowdbPlugin_1 = __importDefault(require("../lowdbPlugin"));
const user_fixture_1 = require("./fixtures/user.fixture");
describe("[TEST] Testing LowDb Plugin", () => {
    let fastify;
    let db;
    beforeAll(async () => {
        fastify = (0, fastify_1.default)();
        fastify.register((0, lowdbPlugin_1.default)({ connectionString: "src/modules/auth/v2/__test__/data/fakeDb.json" }));
        await fastify.ready();
        db = fastify.lowdb.client;
    });
    afterEach(() => {
        db.setState({ users: [] }).write();
    });
    afterAll(() => {
        fastify.close();
    });
    describe("[TEST] Testing lowdb find method", () => {
        test("[FUNCTION] Testing find function while input does not found on database result in undefined", async () => {
            const result = await fastify.lowdb.find(user => user.username === user_fixture_1.CREATE_USER_DATA.username);
            expect(result).toBeUndefined();
        });
        test("[FUNCTION] Testing find function while input matched on database result in user data", async () => {
            db.get("users").push(user_fixture_1.CREATE_USER_DATA).write();
            const result = await fastify.lowdb.find(user => user.username === user_fixture_1.CREATE_USER_DATA.username);
            expect(result).toBeTruthy();
            expect(result.id).toEqual(user_fixture_1.CREATE_USER_DATA.id);
        });
    });
    describe("[TEST] Testing lowdb create method", () => {
        test("[FUNCTION] Testing create function return new user data", async () => {
            jest.spyOn(db, 'write')
                .mockImplementation(() => Promise.resolve());
            const result = await fastify.lowdb.create(user_fixture_1.CREATE_USER_DATA);
            expect(db.write).toHaveBeenCalled();
            expect(result).toBeTruthy();
            expect(result.id).toBe(user_fixture_1.CREATE_USER_DATA.id);
        });
    });
});
//# sourceMappingURL=lowdbPlugin.test.js.map