"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hash_1 = require("@lib/hash");
const error_1 = require("@root/lib/error");
const crypto_1 = require("crypto");
const graphql_1 = require("graphql");
const userResolver = {
    Mutation: {
        async signUp(_, args, { fastify }) {
            const { username, password } = args.user;
            const user = await fastify.lowdb.find(user => user.username === username);
            if (user) {
                throw new graphql_1.GraphQLError(`user ${username} has existed`);
            }
            const hashedPassword = await (0, hash_1.hashData)(password);
            const _user = await fastify.lowdb.create({
                username,
                password: hashedPassword,
                id: (0, crypto_1.randomUUID)()
            });
            delete _user.password;
            return _user;
        }
    },
    Query: {
        async get(_, __, { fastify, req }) {
            if (!req.user) {
                throw new error_1.UnauthenticationError("token invalid");
            }
            const { id, username } = req.user;
            const user = await fastify.lowdb.find(user => user.id === id);
            if (!user) {
                throw new graphql_1.GraphQLError(`user '${username}' does not existed`);
            }
            delete user.password;
            return user;
        },
        async login(_, args, { fastify }) {
            const { username, password } = args.user;
            const user = await fastify.lowdb.find(user => user.username === username);
            if (!user) {
                throw new graphql_1.GraphQLError(`user '${username}' does not existed`);
            }
            const isMatch = await (0, hash_1.compareData)(password, user.password);
            if (!isMatch) {
                throw new graphql_1.GraphQLError(`password does not matched`);
            }
            const tokens = fastify.token.signToken({ username, id: user.id });
            delete user.password;
            return {
                token: {
                    ...tokens
                },
                user: {
                    ...user
                }
            };
        }
    }
};
exports.default = userResolver;
//# sourceMappingURL=userResolver.js.map