"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userResolver = exports.typeDefs = exports.lowdbPlugin = void 0;
const lowdbPlugin_1 = __importDefault(require("./lowdbPlugin"));
exports.lowdbPlugin = lowdbPlugin_1.default;
const typedef_1 = require("./typedef");
Object.defineProperty(exports, "typeDefs", { enumerable: true, get: function () { return typedef_1.typeDefs; } });
const userResolver_1 = __importDefault(require("./userResolver"));
exports.userResolver = userResolver_1.default;
//# sourceMappingURL=index.js.map