import { FastifyPluginCallback } from "fastify";
import { LowdbSync } from "lowdb";
import User from "./user";
export interface DbOptions {
    connectionString: string;
}
export interface ILowdbMethod {
    create(user: User): Promise<User>;
    find(expression: (entity: User) => boolean): Promise<User | undefined>;
    client: LowdbSync<{
        users: User[];
    }>;
}
declare const lowdbPlugin: (dbOptions: DbOptions) => FastifyPluginCallback;
export default lowdbPlugin;
