"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const FileSync_1 = __importDefault(require("lowdb/adapters/FileSync"));
const lowdb_1 = __importDefault(require("lowdb"));
const fastify_plugin_1 = __importDefault(require("fastify-plugin"));
const lowdbPlugin = (dbOptions) => {
    const defaultData = { users: [] };
    const adapter = new FileSync_1.default(dbOptions.connectionString);
    const db = (0, lowdb_1.default)(adapter);
    db.defaults({ ...defaultData }).write();
    const result = (0, fastify_plugin_1.default)((fastify, opts, done) => {
        const _result = {
            async create(user) {
                db.get("users")
                    .push({ ...user })
                    .write();
                return user;
            },
            async find(expression) {
                const entities = db.get("users");
                const index = entities.findIndex(expression).value();
                if (index === -1) {
                    return undefined;
                }
                const result = entities.get(index).value();
                return { ...result };
            },
            client: db
        };
        fastify.decorate("lowdb", _result);
        done();
    });
    return result;
};
exports.default = lowdbPlugin;
//# sourceMappingURL=lowdbPlugin.js.map