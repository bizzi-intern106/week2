import { TokenGeneration, TokenGenerator } from "@lib/token";
import User from "./user";
import { IUserRepository } from "./userRepository";
import { UserResponse } from "@root/resource/userResource";
export interface IUserService {
    get({ username }: Omit<User, 'password'>): Promise<{
        user: UserResponse;
    }>;
    signUp({ username, password }: User): Promise<{
        user: UserResponse;
    }>;
    login({ username, password }: User): Promise<{
        user: UserResponse;
        token: TokenGeneration;
    }>;
}
declare const userService: (userRepository: IUserRepository, tokenGenerator: TokenGenerator) => IUserService;
export default userService;
