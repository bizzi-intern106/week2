"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_fixture_1 = require("./fixtures/user.fixture");
const error_1 = require("@lib/error");
const index_1 = __importDefault(require("@root/index"));
describe("[TEST] UserRouter functionalities", () => {
    let _userService;
    let _tokenGeneration;
    let server;
    beforeAll(async () => {
        const fastify = await (0, index_1.default)();
        server = fastify;
        _userService = fastify.container.resolve("userService");
        _tokenGeneration = fastify.container.resolve("tokenGenerator");
    });
    afterAll(() => {
        server.close();
    });
    describe("[TEST] /v1/auth", () => {
        test("[FUNCTION] Testing GET /v1/auth endpoint with empty authorization header result in unauthentication response", async () => {
            const response = await server.inject({
                method: "GET",
                url: "/v1/auth"
            });
            expect(response.statusCode).toBe(401);
            const json = response.json();
            expect(json.error).toBeTruthy();
        });
        test("[FUNCTION] Testing GET /v1/auth endpoint with token expiration result in unauthentication response", async () => {
            jest.spyOn(_tokenGeneration, 'verifyToken')
                .mockImplementation(() => {
                throw new error_1.UnauthenticationError("");
            });
            const response = await server.inject({
                method: "GET",
                url: "/v1/auth",
                headers: {
                    authorization: "Bearer token"
                }
            });
            expect(response.statusCode).toBe(401);
            const json = response.json();
            expect(json.error).toBeTruthy();
        });
        test("[FUNCTION] Testing GET /v1/auth endpoint while verify token success but username does not found result in bad request response", async () => {
            jest.spyOn(_tokenGeneration, 'verifyToken')
                .mockImplementation(() => ({
                username: user_fixture_1.CREATE_USER_DATA.username,
                id: ""
            }));
            jest.spyOn(_userService, 'get')
                .mockImplementation(() => Promise.reject(new error_1.BadRequestError("")));
            const response = await server.inject({
                method: "GET",
                url: "/v1/auth",
                headers: {
                    authorization: "Bearer token"
                }
            });
            expect(response.statusCode).toBe(400);
            const json = response.json();
            expect(json.error).toBeTruthy();
        });
        test("[FUNCTION] Testing GET /v1/auth endpoint while verify token success result in unauthentication response", async () => {
            jest.spyOn(_tokenGeneration, 'verifyToken')
                .mockImplementation(() => ({
                username: user_fixture_1.CREATE_USER_DATA.username,
                id: ""
            }));
            jest.spyOn(_userService, 'get')
                .mockImplementation(() => Promise.resolve({ user: user_fixture_1.CREATE_USER_DATA }));
            const response = await server.inject({
                method: "GET",
                url: "/v1/auth",
                headers: {
                    authorization: "Bearer token"
                }
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json.user).toBeTruthy();
            expect(json.user.username).toEqual(user_fixture_1.CREATE_USER_DATA.username);
        });
    });
    describe("[TEST] /v1/auth/signup", () => {
        test("[FUNCTION] Testing POST /v1/auth/signup endpoint with user input has existed result in bad request response", async () => {
            jest.spyOn(_userService, 'signUp')
                .mockImplementation(() => Promise.reject(new error_1.BadRequestError("something bad")));
            const response = await server.inject({
                method: "POST",
                url: "/v1/auth/signup",
                payload: user_fixture_1.CREATE_USER_DATA
            });
            expect(response.statusCode).toBe(400);
            const json = response.json();
            expect(json.error).toBeTruthy();
        });
        test("[FUNCTION] Testing POST /v1/auth/signup endpoint with user input does not exist result in ok response", async () => {
            jest.spyOn(_userService, 'signUp')
                .mockImplementation(() => Promise.resolve({ user: user_fixture_1.CREATE_USER_DATA }));
            const response = await server.inject({
                method: "POST",
                url: "/v1/auth/signup",
                payload: user_fixture_1.CREATE_USER_DATA
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json).toBeTruthy();
            expect(json.user.username).toEqual(user_fixture_1.CREATE_USER_DATA.username);
        });
    });
    describe("[TEST] /v1/auth/login", () => {
        test("[FUNCTION] Testing POST /v1/auth/login endpoint with user input has existed result in bad request response", async () => {
            jest.spyOn(_userService, 'login')
                .mockImplementation(() => Promise.reject(new error_1.BadRequestError("something bad")));
            const response = await server.inject({
                method: "POST",
                url: "/v1/auth/login",
                payload: user_fixture_1.CREATE_USER_DATA
            });
            expect(response.statusCode).toBe(400);
            const json = response.json();
            expect(json.error).toBeTruthy();
        });
        test("[FUNCTION] Testing POST /v1/auth/login endpoint with user input does not exist result in ok response", async () => {
            jest.spyOn(_userService, 'login')
                .mockImplementation(() => Promise.resolve({
                user: {
                    username: user_fixture_1.CREATE_USER_DATA.username,
                },
                token: {
                    accessToken: "",
                    refreshToken: ""
                }
            }));
            const response = await server.inject({
                method: "POST",
                url: "/v1/auth/login",
                payload: user_fixture_1.CREATE_USER_DATA
            });
            expect(response.statusCode).toBe(200);
            const json = response.json();
            expect(json).toBeTruthy();
            expect(json.user.username).toEqual(user_fixture_1.CREATE_USER_DATA.username);
        });
    });
});
//# sourceMappingURL=userRouter.test.js.map